import fitz
import sys,re

if(len(sys.argv) < 4):
    sys.exit(-1)

row_amount = 4
col_amount = 5

cards = []

offset_x = 23
offset_y = 23

padding_x = 9
padding_y = 33

width = 135
height = 110

cards = []

doc = fitz.open(sys.argv[1])
with open(f"output.txt", "w") as output:
    for page in range(int(sys.argv[2])-1,int(sys.argv[3])):
        print(f"Page: {page+1}")
        for y in range(col_amount):
            for x in range(row_amount):
                x1 = (padding_x * x if x > 0 else 0) + offset_x + x * width
                y1 = (padding_y * y if y > 0 else 0) + offset_y + y * height
                x2 = offset_x + width + x * width + (padding_x * x if x > 0 else 0)
                y2 = offset_y + height + y * height + (padding_y * y if y > 0 else 0)
                rect = fitz.Rect(x1,y1,x2,y2)
                text = doc[page].get_textbox(rect)
                #text_formated = re.escape(text)
                formatted = re.sub("(\r\n|\r|\n)", " ", text)
                cleaned = re.sub("(\\\)", "", formatted)
                output.write(re.sub("\s+"," ",cleaned) + '\n')