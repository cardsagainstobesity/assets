# PDF Scraper
## How to use

1. (Optional) Create a virtual environment (venv) and activate it.
```
python3 -m venv venv
source venv/bin/activate
```
2. Install the dependencies
```
pip install -r requirements.txt
```
3. Run the script. (Arguments: "Path to PDF file", "starting page", "ending page").
```
# Example 1 (Relative path)
python3 pdf_scraper.py file.pdf 3 28

# Example 2 (Absolute path)
python3 pdf_scraper.py "/path/to/file.pdf" 3 28
```

> Note: This has only been tested with Cards Against Humanity INTL, other official card packs may need some tweaking to get working.