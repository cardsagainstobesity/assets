import { exit } from "process";
import fs from 'fs';
import path from 'path';
import CardPackParser from "../../../Server/src/util/CardPackParser.mjs";

if(!process.env.FOLDER) {
    console.error("Porfavor expecifica la carpeta del pack poniendo FOLDER='path' al principio");
    exit(-1);
}



const pack_folder = process.env.FOLDER;
const white_csv = path.join(pack_folder,"white_cards.csv");
const black_csv = path.join(pack_folder,"black_cards.csv");
const pack_info = path.join(pack_folder,"pack_info.json");
const output_path = path.join(pack_folder,"pack.json");

if(!fs.existsSync(pack_info)|| !fs.existsSync(white_csv) || !fs.existsSync(black_csv)) {
    console.error("La estructura del pack no es correcta");
    console.log("Estructura correcta:\n",
        "\nnew_pack \
          \n  | white_cards.csv \
          \n  | black_cards.csv \
          \n  | pack_info.json \
        "    
    );
    exit(-2);
}

const white_csv_raw = fs.readFileSync(white_csv);
const black_csv_raw = fs.readFileSync(black_csv);
const pack_info_raw = fs.readFileSync(pack_info);

const cardpack = new CardPackParser(JSON.parse(pack_info_raw.toString()),white_csv_raw.toString("utf-8"),black_csv_raw.toString("utf-8"));
cardpack.parse()
.then(pack => {
    fs.writeFile(output_path,JSON.stringify(pack), function(err) {
        if(err) throw err;
        console.log("Creado pack en: ", output_path);
    });
});